JoddEmailSenderJSF

Like all my samples do not forget to retrieve and install https://gitlab.com/omniprof/web_project_dependencies.git before you compile this.

This web app uses a JSF form to collect an email address, subject and message text.
The email field is validated with a custom validator.
The credentials can be found in web.xml. Fill in the XXXXX sections. I recommend a gmail account with security "Less secure app access" set to on.


    <!-- Global email parameters available to all servlets -->
    <context-param>
        <param-name>emailSender</param-name>
        <param-value>XXXXX</param-value>
    </context-param>
    <context-param>
        <param-name>smtpServerName</param-name>
        <param-value>XXXXX</param-value>
    </context-param>
    <context-param>
        <param-name>smtpPassword</param-name>
        <param-value>XXXXX</param-value>
    </context-param>

