package com.kenfogel.joddemailsenderjsf.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import jodd.mail.RFC2822AddressParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom validator that uses the f:validate tag
 * @author Ken Fogel
 */
@FacesValidator("EmailValidator")
public class EmailValidator implements Validator {
    
    private final static Logger LOG = LoggerFactory.getLogger(EmailValidator.class);

    /**
     * Validates if the entered email is valid. Accepts these three parameters
     *
     * @param context
     * @param component
     * @param value
     */
    @Override
    public void validate(FacesContext context, UIComponent component,
            Object value) {
        
        if (value == null) {
            return;
        }
        
        String address = (String) value;

        if (!checkEmail(address)) {
            FacesMessage message = com.kfwebstandard.util.Messages.getMessage(
                    "com.kfwebstandard.bundles.messages", "invalidEmail", null);
            message.setSeverity(FacesMessage.SEVERITY_ERROR);
            throw new ValidatorException(message);
        }
    }

    /**
     * Use the RFC2822AddressParser to validate that the email string could be a
     * valid address. Part of Jodd mail.
     *
     * @param address
     * @return true is OK, false if not
     */
    private boolean checkEmail(String address) {
        return RFC2822AddressParser.STRICT.parseToEmailAddress(address) != null;
    }
    
}
